﻿using System;
using System.Collections.Generic;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Common;
using Nop.Web.Models.Media;
//using Nop.Web.Validators.ShippingCharge;
using FluentValidation.Attributes;

namespace Nop.Web.Models.ShippingCharge
{
    //[Validator(typeof(ShippingChargeValidator))]
    public partial class ShippingChargeModel : BaseNopEntityModel
    {
        public const decimal LBS_KG_RATE = 0.453592M;
        public const decimal INCH_CM_RATE = 2.54M;

        public ShippingChargeModel()
        {
        }

        public string UserName { get; set; }

        public string FullName { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string ServiceType { get; set; }

        public string PkgType { get; set; }

        public string FromZone { get; set; }

        public string ToZone { get; set; }

        public string Weight { get; set; }

        public string Length { get; set; }

        public string Height { get; set; }

        public string Width { get; set; }

        public string Note { get; set; }

        public string Weight_Unit { get; set; }

        public string Length_Unit { get; set; }
        
    }
}