﻿using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.ShippingCharge
{
    public partial class TATModel : BaseNopEntityModel
    {
        public TATModel()
        {
        }

        public string HawbNo { get; set; }        
    }
}