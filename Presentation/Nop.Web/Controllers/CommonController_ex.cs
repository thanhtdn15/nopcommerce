﻿
using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core.Domain.Messages;
using Nop.Services.Customers;
using Nop.Web.Framework.Security;
using Nop.Web.Models.Common;
using Nop.Web.Models.Topics;

namespace Nop.Web.Controllers
{
    public partial class CommonController
    {
        public ActionResult HomePageSliderInfo()
        {
            return View();
        }

        public ActionResult PartnerLink()
        {
            return View();
        }

        public ActionResult MainPageBottom()
        {
            return View();
        }

        public const String OpenAuthorizationPopup_Key = "OpenAuthorizationPopup";
        public const String CancelUrl_Key = "CancelUrl";

        public static void SetAuthorizationRequire(ViewDataDictionary dicViewData, string cancelUrl = "")
        {
            dicViewData[OpenAuthorizationPopup_Key] = true;
            dicViewData[CancelUrl_Key] = cancelUrl;
        }
    }
}
