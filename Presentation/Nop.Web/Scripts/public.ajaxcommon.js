﻿/*
** nopCommerce ajax cart implementation
*/


var AjaxCommon = {
    loadWaiting: false,
    

    init: function () {
        this.loadWaiting = false;
    },

    setLoadWaiting: function (display) {
        displayAjaxLoading(display);
        this.loadWaiting = display;
    },

    //add a product to the cart/wishlist from the catalog pages
    callAjax: function (url, options) {
        if (this.loadWaiting != false) {
            return;
        }
        this.setLoadWaiting(true);

        $.ajax(url, options);
    },

    resetLoadWaiting: function () {
        this.setLoadWaiting(false);       
    }
};