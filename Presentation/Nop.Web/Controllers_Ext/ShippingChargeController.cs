﻿using System;
using System.Web.Mvc;
using Nop.Web.Models.ShippingCharge;

namespace Nop.Web.Controllers
{
    public partial class ShippingChargeController : BasePublicController
    {
        // GET: ShippingCharge
        [AcceptVerbs(HttpVerbs.Get)]
        public ActionResult TrackAndTraceAjax(TATModel model)
        {
            if (String.IsNullOrWhiteSpace(model.HawbNo))
            {
                return Json(new
                {
                    success = false,
                    imgUrl = "",
                    message = "Mã BPBK tra cứu không được trống!"  //String.Join("\n", errorList)
                }, JsonRequestBehavior.AllowGet);
            }

            //TrackAndTraceDTOList lstTAT = null;
            //var err = H2Service.GetTrackAndTrace(model.HawbNo, "vi-VN", out lstTAT);
            //if (err.ID < 0)
            //{
            //    return Json(new
            //    {
            //        success = false,
            //        imgUrl = "",
            //        message = err.Message  //String.Join("\n", errorList)
            //    }, JsonRequestBehavior.AllowGet);
            //}

            //if (lstTAT != null && lstTAT.Count > 0)
            //{
            //    string imgUrl = "";
            //    foreach (var o in lstTAT)
            //    {
            //        if (!string.IsNullOrWhiteSpace(o.YELLOW_LINK))
            //        {
            //            imgUrl = o.YELLOW_LINK;
            //            //break;
            //        }

            //        DateTime d = DateTime.ParseExact(o.EVENT_TIME, "yyyyMMddHHmmss", null);
            //        o.EVENT_TIME = d.ToString("dd/MM/yyyy HH:mm:ss");
            //    }
            //    var qData = (from s in lstTAT
            //                 select new { time = s.EVENT_TIME, desc = s.DESCRIPTION }).ToList();


            //    return Json(new
            //    {
            //        success = true,
            //        imgUrl = imgUrl,
            //        data = qData,
            //        hawbno = model.HawbNo,
            //        message = ""
            //    }, JsonRequestBehavior.AllowGet);
            //}

            return Json(new
            {
                success = false,
                imgUrl = "",
                message = "Không thấy thông tin của BPBK"  //String.Join("\n", errorList)
            }, JsonRequestBehavior.AllowGet);
        }
    }
}